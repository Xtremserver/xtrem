#!/bin/bash
#Script for ServerMessage Editing

echo "clear"                                                                                      >> .bashrc
echo 'echo -e "\e[0m                                                                           "' >> .bashrc
echo 'echo -e "\e[94m      ____  ____  __    ____  ____  _  _  ____  ____  __  __ _  ____      "' >> .bashrc 
echo 'echo -e "\e[94m     (_  _)(  __)(  )  (  __)(_  _)/ )( \(  _ \(  _ \(  )(  ( \/ ___)     "' >> .bashrc
echo 'echo -e "\e[94m       )(   ) _) / (_/\ ) _)   )(  ) \/ ( ) _ ( ) _ ( )( /    /\___ \     "' >> .bashrc
echo 'echo -e "\e[94m      (__) (____)\____/(____) (__) \____/(____/(____/(__)\_)__)(____/     "' >> .bashrc
echo 'echo -e "\e[0m"'                                                                            >> .bashrc
echo 'echo -e "\e[94m                           AutoScriptVPS by Punish                        "' >> .bashrc                                                  
echo -e "\e[0m"
echo -e ""
echo -e ""
echo -e ""
echo -e "READ ME FIRST"
echo -e ""
echo -e "* To Save Changes in Nano Editor *"
echo -e "Press Ctrl+X,"
echo -e "Press Y,"
echo -e "Press Enter,"
echo -e ""
echo -e ""
echo -e "* To Close Nano Editor *"
echo -e "Press Ctrl+Z"
echo -e ""
echo -e ""
echo -e "* To Change Server Message *"
echo -e "Please input the following command:"
echo -e "nano /etc/issue.net"
echo -e ""
echo -e ""